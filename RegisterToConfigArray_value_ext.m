function configuration_array = RegisterToConfigArray_value_ext(configuration_array, register, LowestBit, Bits, Loop, revert)

for i = 1:Loop

    switch register(i).format

      case 'dec'

        if revert
            configuration_array(LowestBit+1:LowestBit+Bits) = dec2bin(str2num(register(i).value), Bits);
        else
            configuration_array(LowestBit+1:LowestBit+Bits) = fliplr(dec2bin(str2num(register(i).value), Bits));
        end

      case 'bin'

        if Bits ~= length(register(i).value)
            error('register %s, bin format', register(i))
        end

        if revert
            configuration_array(LowestBit+1:LowestBit+Bits) = register(i).value;
        else
            configuration_array(LowestBit+1:LowestBit+Bits) = fliplr(register(i).value);
        end

      case 'hex'

        if revert
            configuration_array(LowestBit+1:LowestBit+Bits) = dec2bin(hex2dec(register(i).value), Bits);
        else
            configuration_array(LowestBit+1:LowestBit+Bits) = fliplr(dec2bin(hex2dec(register(i).value), Bits));
        end

      otherwise

    end

    configuration_array(LowestBit + Bits + 1) = register(i).off_PA_HG;
    configuration_array(LowestBit + Bits + 2) = register(i).off_PA_LG;

    LowestBit = LowestBit + Bits + 2;

end