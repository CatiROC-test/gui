function tdc(dirname)

    if nargin==0
        dirname = '.';
    end

    L = 130;

    fprintf('\nComputing TDC analysis.\n')

    load(fullfile(dirname, './results/decoded_dis.mat'), ...
         'AllCoarseTimes', 'AllFineTimes', 'Channel', 'AllEdges')

    %% Avoid odd values
    if mod(size(AllFineTimes(:, 1), 1) , 2) == 1
        AllFineTimes   = AllFineTimes(1:end-1, :);
        AllCoarseTimes = AllCoarseTimes(1:end-1, :);
        AllEdges       = AllEdges(1:end-1, :);
    end

    h = figure('Visible', 'off'); hold on, zoom on, grid minor
    xlabel('Bin'), ylabel('Value')
    title('Event Number Diff Histogram, all channels')

    all_channels = unique(Channel)';
    fprintf('\nComputing histogram (between ch. %i and ch. %i) for channel ... ', ...
    all_channels(1), all_channels(end))

    HistogramTDC = zeros(L, length(all_channels));

    for i = 1:length(all_channels)

        ch_nb = uint16(all_channels(i));

        fprintf('%i ', ch_nb)

        toto = diff(...
        reshape(...
        AllFineTimes(:, i) + 24*AllCoarseTimes(:, i), 2, []), 1);

        [n,x] = hist(toto, 1:L);

        if any(toto<1)
            fprintf('\nFound negative time, channel %i.\n', ch_nb)
        end

        HistogramTDC(:,i) = n;

    end

    fprintf('\n')

    %% plot
    bar(x, HistogramTDC)

    set(gca, 'YScale', 'log')
    set(gca, 'XLim', [0 20])
    tmp = get(gca, 'XLim');
    set(gca, 'XLim', [0 tmp(2)])

    saveas(h, fullfile(dirname, './results/tdc.fig'))
    saveas(h, fullfile(dirname, './results/tdc.png'))

    close(h)

    %% Save data
    save(fullfile(dirname, ...
    './results/HistogramTDC.mat'), 'HistogramTDC')

    [x,y]= meshgrid(1:size(HistogramTDC,1), 1:size(HistogramTDC,2));
    hh = figure('Visible', 'off');
    surf(y', x', HistogramTDC), view(2), shading flat, axis tight
    colorbar, colormap hot

    saveas(hh, fullfile(dirname, './results/tdc.fig') )
    saveas(hh, fullfile(dirname, './results/tdc.png') )

    xlabel('Electronic Channel number')
    ylabel('TDC Channel (ns.)')
    title('TDC Timing')
    axis square

    close(hh)

    fprintf('Done.\n')

end