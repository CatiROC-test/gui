function SetColor(Handle)

    set(Handle(1), 'Color', 'b', 'LineStyle', '-')
    set(Handle(2), 'Color', 'b', 'LineStyle', '--')

    set(Handle(3), 'Color', 'r', 'LineStyle', '-')
    set(Handle(4), 'Color', 'r', 'LineStyle', '--')

    set(Handle(5), 'Color', 'g', 'LineStyle', '-')
    set(Handle(6), 'Color', 'g', 'LineStyle', '--')

    set(Handle(7), 'Color', 'c', 'LineStyle', '-')
    set(Handle(8), 'Color', 'c', 'LineStyle', '--')

    set(Handle(9), 'Color', 'm', 'LineStyle', '-')
    set(Handle(10), 'Color', 'm', 'LineStyle', '--')

    set(Handle(11), 'Color', 'y', 'LineStyle', '-')
    set(Handle(12), 'Color', 'y', 'LineStyle', '--')

    set(Handle(13), 'Color', 'k', 'LineStyle', '-')
    set(Handle(14), 'Color', 'k', 'LineStyle', '--')

    set(Handle(15), 'Color', [0.9290 0.6940 0.1250], 'LineStyle', '-')
    set(Handle(16), 'Color', [0.9290 0.6940 0.1250], 'LineStyle', '--')

    set(Handle(17), 'Color', 'b', 'LineStyle', '-')
    set(Handle(18), 'Color', 'b', 'LineStyle', '--')

    set(Handle(19), 'Color', 'r', 'LineStyle', '-')
    set(Handle(20), 'Color', 'r', 'LineStyle', '--')

    set(Handle(21), 'Color', 'g', 'LineStyle', '-')
    set(Handle(22), 'Color', 'g', 'LineStyle', '--')

    set(Handle(23), 'Color', 'c', 'LineStyle', '-')
    set(Handle(24), 'Color', 'c', 'LineStyle', '--')

    set(Handle(25), 'Color', 'm', 'LineStyle', '-')
    set(Handle(26), 'Color', 'm', 'LineStyle', '--')

    set(Handle(27), 'Color', 'y', 'LineStyle', '-')
    set(Handle(28), 'Color', 'y', 'LineStyle', '--')

    set(Handle(29), 'Color', [0.9290 0.6940 0.1250], 'LineStyle', '-')
    set(Handle(30), 'Color', [0.9290 0.6940 0.1250], 'LineStyle', '--')

    set(Handle(31), 'Color', [0.4940 0.1840 0.5560], 'LineStyle', '-')
    set(Handle(32), 'Color', [0.4940 0.1840 0.5560], 'LineStyle', '--')
end