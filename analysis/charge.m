function charge(dirname)
% Compute charge histograms

    if nargin==0
        dirname = '.';
    end

    fprintf('\nComputing charge analysis.\n')

    %% Load data
    load(fullfile(dirname, './results/decoded_phy.mat'), ...
         'Channel', 'EventCounter', 'Charge', 'NbAsics');

    %% Preallocate arrays
    HistogramCharge = zeros(2^10, 16*2*NbAsics, 'uint64');

    %% Figure to hold histograms
    HistogramChargeFigure = figure('Visible', 'off', ...
                                   'CloseRequestFcn', 'closereq',  ...
                                   'Numbertitle', 'off', ...
                                   'Menubar', 'none', ...
                                   'Toolbar', 'figure', ...
                                   'Name', sprintf('%s', 'ADC Charge' ));

    HistogramChargeFigureAxis = gca;
    PlotHandleHistogramCharge = plot(HistogramCharge);
    SetColor(PlotHandleHistogramCharge)
    xlabel('Channel'), ylabel('Counts'), title('Charge Histogram')
    zoom on, grid minor, hold on

    if NbAsics==1
        legend('0','0','1','1','2','2','3','3','4','4','5','5','6','6','7','7','8','8',...
               '9','9','10','10','11','11','12','12','13','13', ...
               '14','14','15','15','Location', ...
               'eastoutside')
    end

    all_channels = unique(Channel)';

    fprintf('\nComputing histogram (between ch. %i and ch. %i) for channel ... ', ...
            all_channels(1), all_channels(end))

    for i = 1:length(all_channels)

        ch_nb = uint16(all_channels(i));

        fprintf('%i ', ch_nb)

        tmp    = (Channel == ch_nb);
        tmp2   = logical(mod(EventCounter,2));
        index1 = tmp & tmp2;
        index2 = tmp & ~tmp2;

        HistogramCharge(:, 2*ch_nb+1) = HistogramCharge(:, 2*ch_nb+1) + ...
            uint64(hist(Charge(index1), 0:1023))';

        HistogramCharge(:, 2*ch_nb+2) = HistogramCharge(:, 2*ch_nb+2) + ...
            uint64(hist(Charge(index2), 0:1023))';

        set(PlotHandleHistogramCharge(2*ch_nb+1), 'YData', ...
                          HistogramCharge(:, 2*ch_nb+1))
        set(PlotHandleHistogramCharge(2*ch_nb+2), 'YData', ...
                          HistogramCharge(:, 2*ch_nb+2))

    end % for channels

    fprintf('\n')

    set(HistogramChargeFigureAxis, 'XLim', ...
                      [find(sum(HistogramCharge, 2),1,'first')-10 ...
                       find(sum(HistogramCharge, 2),1,'last')+10])
    set(HistogramChargeFigureAxis, 'YScale', 'log')

    saveas(HistogramChargeFigure, fullfile(dirname, './results/charge.fig') )
    saveas(HistogramChargeFigure, fullfile(dirname, './results/charge.png') )

    close(HistogramChargeFigure)

    save(fullfile(dirname, './results/HistogramCharge.mat'), ...
         'HistogramCharge')

    %% Figure to hold bidim histograms

    %% limit the max piedestal
    data = HistogramCharge(1:200,:);

    %% limit the max to best colormap
    ref =  3*mean(data(data>5));
    data(data>ref) = ref;

    [x,y]= meshgrid(1:size(data,1), 1:size(data,2));
    hh = figure('Visible', 'off');
    surf(y', x', data), view(2), shading flat, axis tight
    colorbar, colormap hot

    saveas(hh, fullfile(dirname, './results/charge_bidim.fig') )
    saveas(hh, fullfile(dirname, './results/charge_bidim.png') )

    close(hh)

end