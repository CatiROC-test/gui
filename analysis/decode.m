function decode(header, dirname, NbAsics)

    if nargin==0
        header = 1;
        dirname = '.';
        NbAsics = 8;
    end

% * Read config file

    GlobalParameters = GetGlobalParameters();
    configuration = GetConfig(NbAsics);

    %% Compute how many events will be ignored at the beginning of the
    %% acquisition. These events originate from the ASIC (as much as we know) and
    %% must be removed propertly.
    if isfield(GlobalParameters, "IgnoreFakeEvents") && ...
            GlobalParameters.IgnoreFakeEvents

        %% Assumes that there is one fake event by half asic (group of 8
        %% channels). One needs to stimate how many of them to ignore from the
        %% config files
        NbEventsToIgnore = 0;
        %% for each asic
        for i = 1:NbAsics
            %% if asic enabled
            if bitget(hex2dec(GlobalParameters.EnabledASICs), i)
                %% first 8 channels
                if ~(255 == hex2dec(configuration(i).Mask.value(1:2)))
                    NbEventsToIgnore = NbEventsToIgnore + 1;
                end
                %% remaining 8 channels
                if ~(255 == hex2dec(configuration(i).Mask.value(3:4)))
                    NbEventsToIgnore = NbEventsToIgnore + 1;
                end
            end
        end
        fprintf('\n\t%i Fake events from beginning of acquisition will be ignored.\n', NbEventsToIgnore)

    end

% * Read raw file

    BinFile = dir(fullfile(dirname, '*.bin'));
    fprintf('\nData file is %s\n\n', fullfile(BinFile.name))

    %% Read data from file
    fprintf('Reading bin file ... \n\n')
    fid = fopen(fullfile(dirname, BinFile.name), 'r', 'b');

% ** Read header if necessary
    if header
        AcqRawDataFormatID = decode_header(fid);
    end

% ** Read all payload

    fprintf('\nReading payload ... ')
    if GlobalParameters.NbEventsToAnalyse ==  -1
        GlobalParameters.NbEventsToAnalyse = inf;
    end
    data = fread(fid, GlobalParameters.NbEventsToAnalyse*8, 'uint8=>uint32');
    data = reshape(data, 10, [])';

    fclose(fid);
    fprintf('    Done.\n')

% * Decode

    fprintf('\nDecoding payload ... \n')

    tmp_ = find(data(:,10) ~= GlobalParameters.BoardId);
    if ~isempty(tmp_)
        data(tmp_, :) = [];
    end

    %% left most 32 bits in a single word
    Biggest32BitsWord = (data(:,1)*2^24 + ...
                         data(:,2)*2^16 + ...
                         data(:,3)*2^8 + ...
                         data(:,4));

    %% right most 32 bits in a single word
    Lowest32BitsWord = (data(:,5)*2^24 + ...
                        data(:,6)*2^16 + ...
                        data(:,7)*2^8 + ...
                        data(:,8));

    %% Asic number
    AsicNb  = uint8(data(:,9));

    %% Event kind
    EventKind = uint8(bitshift(Biggest32BitsWord, -30));

    %% Save decoded infos for next time
    save(fullfile(dirname, './results/decoded_phy.mat'), ...
         'AsicNb', 'EventKind', 'NbAsics');
    save(fullfile(dirname, './results/decoded_dis.mat'), ...
         'AsicNb', 'EventKind', 'NbAsics');

    AsicNb = data(:,9);

% ** Physical Events

    fprintf('\nDecoding physical data ... ')

    EventKindPhysicalIndex = (EventKind==0);

    if any(EventKindPhysicalIndex)

        tmp          = Biggest32BitsWord(EventKindPhysicalIndex);

        %% Remove fake events
        if isfield(GlobalParameters, "IgnoreFakeEvents") && ...
                GlobalParameters.IgnoreFakeEvents
            FakeIndicesToRemove = find(tmp==0, 16, 'first');
            tmp(FakeIndicesToRemove) = [];
            AsicNb_ = AsicNb(EventKindPhysicalIndex);
            AsicNb_(FakeIndicesToRemove) = [];
        end

        CoarseTime   = bitand(tmp, uint32(2^26-1));

        Channel      = uint8(16*AsicNb_ + bitand(bitshift(tmp, -26), uint32(15)));

        tmp          = Lowest32BitsWord(EventKindPhysicalIndex);

        %% Remove fake events
        if isfield(GlobalParameters, "IgnoreFakeEvents") && ...
                GlobalParameters.IgnoreFakeEvents
            tmp(FakeIndicesToRemove) = [];
        end

        FineTime     = uint16(bitand(tmp, uint32(2^10-1)));

        Charge       = uint16(bitand(bitshift(tmp, -10), uint32(2^10-1)));

        EventCounter = uint16(bitand(bitshift(tmp, -20), uint32(2^11-1)));

        %% save memory
        clear tmp

    else

        CoarseTime = -1;
        Channel = -1;
        Charge = -1;
        FineTime = -1;
        EventCounter = -1;

    end

    fprintf('Done.\n')

    %% Save decoded infos on disk and remove from memory
    save(fullfile(dirname, './results/decoded_phy.mat'), ...
         'Channel', 'EventCounter', 'FineTime', 'Charge', 'CoarseTime', '-append');
    clear Channel EventCount FineTime Charge CoarseTime

% ** Discriminator Events

    fprintf('\nDecoding discriminator data ... ')

    EventKindDiscriminatorIndex = (EventKind==2);

    if any(EventKindDiscriminatorIndex)

        tmp          = Biggest32BitsWord(EventKindDiscriminatorIndex);

        %% Omega test card
        if NbAsics==1

            EventCounter = bitand(tmp, uint32(2^25-1));

            Channel      = uint8(16*AsicNb(EventKindDiscriminatorIndex) + bitand(bitshift(tmp, -25), uint32(15)));

            Edge         = logical(bitand(bitshift(tmp, -29), uint32(1)));

            Interval     = zeros(size(EventCounter), 'uint8');

        %% Juno board, data format v3
        elseif AcqRawDataFormatID == 3

            EventCounter = bitand(tmp, uint32(2^25-1));

            Channel      = uint8(16*AsicNb(EventKindDiscriminatorIndex) + bitand(bitshift(tmp, -25), uint32(15)));

            Edge         = logical(bitand(bitshift(tmp, -29), uint32(1)));

            Interval     = zeros(size(EventCounter), 'uint8');

        %% Juno board, data format v4
        elseif AcqRawDataFormatID == 4

            EventCounter = bitand(bitshift(tmp, -7), uint32(2^18-1));

            Channel      = uint8(16*AsicNb(EventKindDiscriminatorIndex) + bitand(bitshift(tmp, -25), uint32(15)));

            Edge         = logical(bitand(bitshift(tmp, -29), uint32(1)));

            Interval     = bitand(tmp, uint32(2^7-1));

        end

        tmp          = Lowest32BitsWord(EventKindDiscriminatorIndex);

        %% Omega test card
        if NbAsics==1

            CoarseTime   = tmp;

            FineTime     = zeros(size(CoarseTime), 'uint32');

            %% Juno board, data format v3
        else

            CoarseTime   = bitshift(tmp, -6);

            FineTime     = uint8(bitand(tmp, uint32(2^6-1)));

        end

        %% save memory
        clear tmp

    else

        Channel      = -1;
        EventCounter = -1;
        Edge         = -1;
        FineTime     = -1;
        CoarseTime   = -1;

    end

    fprintf('Done.\n')

    %% Save decoded infos on disk and remove from memory
    save(fullfile(dirname, './results/decoded_dis.mat'), ...
         'FineTime', 'Channel', 'EventCounter', 'Edge', 'CoarseTime', '-append');
    clear Channel EventCount FineTime Charge CoarseTime

% ** Special Events

% EventKindSpecialIndex = (EventKind==1);
%
% if any(EventKindSpecialIndex)
%
%     TriggerRate = double(bitand(Lowest32BitsWord(EventKindSpecialIndex), uint32(2^16-1)))/10;
%     TriggerRateChannel = double(bitand(bitshift(Lowest32BitsWord(EventKindSpecialIndex), -16), uint32(31)) + 1);
%
%     if length(TriggerRate) < 32
%     warning('\n\tNumber of Special events received is %i', length(TriggerRate))
%     TriggerRate = 0;
%     else
%         ValidChannels = find(TriggerRate>0);
%         TriggerRate = TriggerRate(ValidChannels);
%         TriggerRateChannel = TriggerRateChannel(ValidChannels);
%     end
%
% end