function coarse_time(type, dirname)

    if nargin==0
        error('At least tell me if I do have to compte phy or dis data ... !')
    elseif nargin==1
        dirname = '.';
    end

% Plot Diff of coarse time by channel
% Should be equal to event rate

    fprintf('\nComputing coarse time analysis.\n')

    if strcmp('phy', type)
        load(fullfile(dirname, './results/decoded_phy.mat'), ...
             'Channel', 'CoarseTime');
    elseif strcmp('dis', type)
        load(fullfile(dirname, './results/decoded_dis.mat'), ...
             'Channel', 'CoarseTime', 'Edge');
    end

    if Channel == -1
        fprintf('\nNo Channel data found. Aborting.\n')
        return
    end

    h = figure('Visible', 'off'); hold on, zoom on, grid minor
    xlabel('Bin'), ylabel('Value')
    title('CoarseTime Diff Histogram, all channels')

    all_channels = unique(Channel)';

    fprintf('\nComputing histogram (between ch. %i and ch. %i) for channel ... ', ...
            all_channels(1), all_channels(end))

    MinLength = 1e30;

    for i = 1:length(all_channels)

        ch_nb = uint16(all_channels(i));

        fprintf('%i ', ch_nb)

        %% Channel i coarse time
        CoarseTime_ch{i} = ...
            convert_overflowing_vector(CoarseTime(Channel==ch_nb), 26);

        if strcmp('dis', type)
            Edge_ch{i} = Edge(Channel==ch_nb);
        end

        if length(CoarseTime_ch{i}) < MinLength
            MinLength = length(CoarseTime_ch{i});
        end

        %% Compute diff
        DiffCoarseTime_ch{i} = diff(CoarseTime_ch{i}); % compute diff

% Negative increases are all tagged as 2000
        DiffCoarseTime_ch{i}(DiffCoarseTime_ch{i}<=0)=0;
        DiffCoarseTime_ch{i}(DiffCoarseTime_ch{i}>5e4)=5e4;
% Compute histogram
        [n, x] = hist(double(DiffCoarseTime_ch{i}), max(DiffCoarseTime_ch{i})-min(DiffCoarseTime_ch{i})+1);
% Update plot figure
        semilogy(x+i*10,n);
    end

    fprintf('\n')

    set(gca, 'YScale', 'log')
    set(gca, 'XLim', [0 5e4+1])

    if strcmp('phy', type)
        saveas(h, fullfile(dirname, './results/CoarseTime_diff_phy.fig'))
        saveas(h, fullfile(dirname, './results/CoarseTime_diff_phy.png'))
    elseif strcmp('dis', type)
        saveas(h, fullfile(dirname, './results/CoarseTime_diff_dis.fig'))
        saveas(h, fullfile(dirname, './results/CoarseTime_diff_dis.png'))
    end

    close(h)

    AllCoarseTimes = zeros(MinLength, length(all_channels));
    for i = 1:length(all_channels)
        AllCoarseTimes(:,i) = CoarseTime_ch{i}(1:MinLength);
    end

    if strcmp('dis', type)
        AllEdges = zeros(MinLength, length(all_channels));
        for i = 1:length(all_channels)
            AllEdges(:,i) = Edge_ch{i}(1:MinLength);
        end
    end

    if strcmp('phy', type)
        save(fullfile(dirname, './results/decoded_phy.mat'), ...
             'AllCoarseTimes', '-append');
    elseif strcmp('dis', type)
        save(fullfile(dirname, './results/decoded_dis.mat'), ...
             'AllCoarseTimes', '-append');
        save(fullfile(dirname, './results/decoded_dis.mat'), ...
             'AllEdges', '-append');
    end

end




%% Test 3

% Plot Diff of course time by channel
% Should be equal to event rate

% clear CoarseTime_ch DiffCoarseTime_ch
% h = figure('Visible', 'on'); hold on, zoom on, grid minor
% xlabel('Event'), ylabel('Value'), title('CoarseTime Diff, all channels')
% for i = 1:16
% % Channel i event counter
%     CoarseTime_ch{i} = double(CoarseTime(Channel==i))';
% % compute diff
%     DiffCoarseTime_ch{i} = diff(CoarseTime_ch{i});
% % when diff equals -2047 there is an overflow, set to 1
%     DiffCoarseTime_ch{i}(DiffCoarseTime_ch{i}==-67107498)=0;
%     DiffCoarseTime_ch{i}(DiffCoarseTime_ch{i}>4e3)=-300;
% % plot
%     plot(DiffCoarseTime_ch{i}+10*i);
% end
% axis tight
%
% saveas(h, '../results/CoarseTime_diff.fig')
% saveas(h, '../results/CoarseTime_diff.png')
% close(h)
%
%
%
% clear CoarseTime_ch
% h = figure('Visible', 'on'); hold on, zoom on, grid minor
% xlabel('Event'), ylabel('Value'), title('CoarseTime, all channels')
% for i = 1:16
% % Channel i event counter
%     CoarseTime_ch{i} = double(CoarseTime(Channel==i))';
% % plot
%     plot(CoarseTime_ch{i}+5e5*i);
% end
% axis tight
%
% saveas(h, '../results/CoarseTime.fig')
% saveas(h, '../results/CoarseTime.png')
% close(h)