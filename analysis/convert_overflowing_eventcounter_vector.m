function eventcounter = convert_overflowing_eventcounter_vector(eventcounter)

% prefer row vectors
    eventcounter = eventcounter(:)';

% better use 64 non signed bits
    eventcounter = uint64(eventcounter);

% Increase vector
    DiffVector = [1 diff(eventcounter)];

% I assume here that negative increases on non signed data corresponds to
% an overflow

% Only consider negative increases higher than a threshold, to avoid
% initial wrong data
    Indices = find(DiffVector==0);
    Indices = Indices(Indices>5);

% create the new vector to compensate overflows
    DiffVector = zeros(size(DiffVector), 'uint64');
    DiffVector(Indices) = 2048;

% add previous to initial eventcounter
    eventcounter = eventcounter + cumsum(DiffVector);

end