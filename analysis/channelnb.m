function channelnb(type, dirname)
% Compute

    if nargin==0
        error('At least tell me if I do have to compte phy or dis data ... !')
    elseif nargin==1
        dirname = '.';
    end

    fprintf('\nComputing channel nb analysis ... ')

    if strcmp('phy', type)
        load(fullfile(dirname, './results/decoded_phy.mat'), ...
             'Channel', 'EventCounter', 'NbAsics');
    elseif strcmp('dis', type)
        load(fullfile(dirname, './results/decoded_dis.mat'), ...
             'Channel', 'Edge', 'NbAsics');
        EventCounter = Edge;
    end

    if Channel == -1
        fprintf('\nNo Channel data found. Aborting.\n')
        return
    end

    HistogramChannelNb = zeros(2, 16*NbAsics, 'uint64');

    index1 = (mod(EventCounter, 2) == 1);
    index2 = (mod(EventCounter, 2) == 0);

    [n1, ~] = hist(Channel(index1), 0:NbAsics*16-1);
    [n2, ~] = hist(Channel(index2), 0:NbAsics*16-1);

    HistogramChannelNb(1, :) = n1;
    HistogramChannelNb(2, :) = n2;

    HistogramChannelNbFigure = figure('Visible', 'off', ...
                                      'CloseRequestFcn', 'closereq',  ...
                                      'Numbertitle', 'off', ...
                                      'Menubar', 'none', ...
                                      'Toolbar', 'figure', ...
                                      'Name', sprintf('%s', 'Channel Nb' ));

    bar(HistogramChannelNb');
    HistogramChannelNb

%     set(PlotHandleHistogramChannelNb(1), 'YData', n1, 'XData', h1)
%     set(PlotHandleHistogramChannelNb(2), 'YData', n2, 'XData', h2)

    xlabel('Channel Nb'), ylabel('Counts')
    title(sprintf('ChannelNb Histogram - %s', type))
    zoom on, grid minor, hold on

    set(gca, 'XLim', [0 129])
    set(gca, 'YScale', 'lin')

    if strcmp('phy', type)
        saveas(gcf, fullfile(dirname, './results/channelnb.fig') )
        saveas(gcf, fullfile(dirname, './results/channelnb.png') )
    elseif strcmp('dis', type)
        saveas(gcf, fullfile(dirname, './results/channelnb_discri.fig') )
        saveas(gcf, fullfile(dirname, './results/channelnb_discri.png') )
    end

    close(HistogramChannelNbFigure)

    if strcmp('phy', type)
        save(fullfile(dirname, './results/HistogramChannelNb.mat'), 'HistogramChannelNb')
    elseif strcmp('dis', type)
        save(fullfile(dirname, './results/HistogramChannelNb_discri.mat'), 'HistogramChannelNb')
    end

    fprintf('Done.\n')

    end
