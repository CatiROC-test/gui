function varargout = catirocgui(varargin)
% CATIROCGUI MATLAB code for catirocgui.fig
%      CATIROCGUI, by itself, creates a new CATIROCGUI or raises the existing
%      singleton*.
%
%      H = CATIROCGUI returns the handle to a new CATIROCGUI or the handle to
%      the existing singleton*.
%
%      CATIROCGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CATIROCGUI.M with the given input arguments.
%
%      CATIROCGUI('Property','Value',...) creates a new CATIROCGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before catirocgui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to catirocgui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help catirocgui

% Last Modified by GUIDE v2.5 16-Nov-2017 11:39:33

% Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @catirocgui_OpeningFcn, ...
                       'gui_OutputFcn',  @catirocgui_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
% End initialization code - DO NOT EDIT

%% GUI

function catirocgui_OpeningFcn(hObject, eventdata, handles, varargin)

    if ~ isdeployed
        addpath(fullfile (getcurrentdir, 'ipeak7'))
        addpath(fullfile (getcurrentdir, 'configuration'))
        addpath(fullfile (getcurrentdir, 'analysis'))
    end

    reset(RandStream.getGlobalStream,sum(100*clock))
    handles = system_initialize(handles);

% Choose default command line output for catirocgui
    handles.output = hObject;

    handles.GeneralPurposeCounterSCurve = 0;

% Update handles structure
    guidata(hObject, handles);

function varargout = catirocgui_OutputFcn(hObject, eventdata, handles)

% Get default command line output from handles structure
    varargout{1} = handles.output;
% varargout{1} = handles;


%% Close

function close_tag_Callback(hObject, eventdata, handles)

    if isfield(handles, 'digi')
        handles.digi.delete
    end
    delete(gcf)

function figure1_CloseRequestFcn(hObject, eventdata, handles)

    if isfield(handles, 'digi')
        handles.digi.delete
    end
    delete(hObject);

function ana_tag_Callback(hObject, eventdata, handles)

function runit_tag_Callback(hObject, eventdata, handles)

% Temporary workaround to bug in 2018.a, see http://bit.ly/2IIOAew
% dirname = uigetdir('.', 'toto');
    dirname = uigetdir_workaround('.', 'toto');
    if isempty(dirname)
        return
    end
    config = GetGlobalParameters;
    fprintf('\nAnalysing for %i asics\n', config.nb_asics)
    runit(config.nb_asics, 1, dirname)

%% Configure

function configure_tag_Callback(hObject, eventdata, handles)

    if isfield(handles, 'digi')
        %% Update contents of global params config file
        handles.digi.Cards(1).GlobalParameters = GetGlobalParameters;
        %% Update configuration
        handles.digi.Cards(1) = handles.digi.Cards(1).UpdateConfig;
        %% Configure ASIC
        [handles.digi.Cards(1), ConfigureASICWrong] = ...
            handles.digi.Cards(1).ConfigureASIC;
        iConfigureASICWrong = 1;
        if ConfigureASICWrong
            while (ConfigureASICWrong && iConfigureASICWrong < 5)
                fprintf('\n\tRetrying ConfigureASIC, retry nb %i:\n', ...
                        iConfigureASICWrong)
                [handles.digi.Cards(1), ConfigureASICWrong] = ...
                    handles.digi.Cards(1).ConfigureASIC;
                iConfigureASICWrong = iConfigureASICWrong + 1;
            end
        end
        guidata(hObject, handles);
    end


%% Acquire

function acquire_tag_Callback(hObject, eventdata, handles)

    if ~get(gcbo,'Value')
        fprintf('\n\t*********************\n')
        fprintf('\n\tStopping Acquisition.\n')
        fprintf('\n\t*********************\n')
        msgbox('Stopping Acquisition.')
        return
    else
        fprintf('\n\t------------------------\n')
        fprintf('\n\tStarting Acquisition ...\n')
        fprintf('\n\t------------------------\n')
    end

    if isfield(handles, 'digi')
        handles.digi.Cards(1).GetASICData(0, handles);
    end

function force_trigger_tag_Callback(hObject, eventdata, handles)

function force_trigger_value_tag_Callback(hObject, eventdata, handles)

function save_data_tag_Callback(hObject, eventdata, handles)

function adc_charge_tag_Callback(hObject, eventdata, handles)

function adc_time_tag_Callback(hObject, eventdata, handles)

function acq_log_tag_Callback(hObject, eventdata, handles)

function auto_scale_tag_Callback(hObject, eventdata, handles)


%% Tools

function tools_bandwidth_tag_Callback(hObject, eventdata, handles)

    if isfield(handles, 'digi')
        handles.digi.Cards(1).test_bandwidth(1, 10);
    end

function tools_datavalid_tag_Callback(hObject, eventdata, handles)

    if isfield(handles, 'digi')
        handles.digi.Cards(1).test_command(100, 1);
    end

function tools_timing_inl_tag_Callback(hObject, eventdata, handles)


    if isfield(handles, 'digi')
        handles.digi.Cards(1).TimeINLMeasurement(handles);
    end

%% Oscillo

function oscillo_chgrp_1_Callback(hObject, eventdata, handles)

function oscillo_chgrp_1_CreateFcn(hObject, eventdata, handles)

    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function oscillo_chgrp_2_Callback(hObject, eventdata, handles)

function oscillo_chgrp_2_CreateFcn(hObject, eventdata, handles)

    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function oscillo_chgrp_3_Callback(hObject, eventdata, handles)

function oscillo_chgrp_3_CreateFcn(hObject, eventdata, handles)

    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function oscillo_chgrp_4_Callback(hObject, eventdata, handles)

function oscillo_chgrp_4_CreateFcn(hObject, eventdata, handles)

    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function oscillo_tag_Callback(hObject, eventdata, handles)

    if ~get(gcbo,'Value'), return, end

    if get(handles.oscillo_chgrp_1, 'Value') == 1 && ...
            get(handles.oscillo_chgrp_2, 'Value') == 1 && ...
            get(handles.oscillo_chgrp_3, 'Value') == 1 && ...
            get(handles.oscillo_chgrp_4, 'Value') == 1
        msgbox('Select at least one channel')
        return
    end

    tmp = [0 1 2 4 8];

    if isfield(handles, 'digi')
        handles.digi.Cards(1).GetADCData(...
            tmp(get(handles.oscillo_chgrp_1, 'Value')), ...
            tmp(get(handles.oscillo_chgrp_2, 'Value')), ...
            tmp(get(handles.oscillo_chgrp_3, 'Value')), ...
            tmp(get(handles.oscillo_chgrp_4, 'Value')), ...
            handles);
    end

function scurve_tag_Callback(hObject, eventdata, handles)

    if ~get(gcbo,'Value'), return, end

    if isfield(handles, 'digi')
        handles.digi.Cards(1).GetSCurve(handles);
    end