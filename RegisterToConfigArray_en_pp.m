function configuration_array = RegisterToConfigArray_en_pp(configuration_array, register, LowestBit)

configuration_array(LowestBit + 1) = register.en;
configuration_array(LowestBit + 2) = register.pp;
