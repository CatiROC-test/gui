function displayfig()

answer = 'Yes';

while strcmp(answer,'Yes')

    [filename, pathname, ~] = ...
        uigetfile( {'*.fig','Figures (*.fig)'}, 'Pick a file');

    open(fullfile(pathname, filename))
    zoom on

    answer = questdlg('Another one ?', 'Answer!', 'Yes', 'No', 'Yes');

end