function AcqRawDataFormatID = GetAcqRawDataFormatID(obj)
% GETFIRMWAREID - Read the AcqRawDataFormatID byte from the firmware

    if obj.GetIsOpen

        obj.PurgeUSB;
        obj.SendCommand([11,255]); % command "0B"
        AmountInRxQueue = obj.GetStatusUSB;

        if AmountInRxQueue ~= 1
            fprintf(['\n\tGetAcqRawDataFormatID - Warning: %i bytes available\n'], AmountInRxQueue)
        end

        lpBufferPtr = libpointer('voidPtr', zeros(AmountInRxQueue, 1, 'uint8'));
        el_read_ptr = libpointer('ulongPtr',uint32(0));

        [status, ~, el_read] = calllib('libftdi', 'ReadUSB', ...
                                       obj.DevNumber, ...
                                       lpBufferPtr, ...
                                       AmountInRxQueue, ...
                                       el_read_ptr);

        if status ~= 0 || el_read ~= 1
            fprintf('\n\tGetASICData - ReadFirmID - Warning: %s bytes read is %i\n', obj.status_chain(status), el_read),
            drawnow
        end
        AcqRawDataFormatID = lpBufferPtr.Value;

        if obj.GlobalParameters.verbose
            fprintf('\n\tAcqRawDataFormatID is %i\n', AcqRawDataFormatID)
        end

    else

        error('READUSB: Device not open.')

    end
