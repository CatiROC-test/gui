
function PurgeUSB(obj)
% PURGEUSB - Remove any data left in the bus

%    PURGEUSB - Send a null command to the board and trash any remaining data in the bus

    if obj.GetIsOpen

        calllib('libftdi', 'PurgeRxTxUSB', obj.DevNumber);

        if obj.GlobalParameters.verbose
            fprintf('\n\tUSB purge device %i done.\n', obj.DevNumber)
        end

    else

        error('PURGEUSB: Device not open.')

    end