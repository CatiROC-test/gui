function obj = InitUSB(obj)
% Init USB

%% Initialize USB

    ft_status   = calllib('libftdi', 'InitializeUSB', obj.DevNumber);
    fprintf('\n\n\tInit USB ... device %i: %s\n', obj.DevNumber, ...
            obj.status_chain(ft_status))
    [obj.SerialNumber, obj.Description] = obj.GetSerialDesc;


    %% Configure USB

    ft_status = calllib('libftdi', 'ConfigureUSB', obj.DevNumber);
    fprintf('\n\tInit USB ... device %i configured: %s\n', ...
            obj.DevNumber, ...
            obj.status_chain(ft_status))

% unloadlibrary('libftdi')

% Configure Board
%    ft_status = calllib('libftdi', 'ConfigureBoard',obj.DevNumber);
%    fprintf('\n\tInit USB ... board configured %s\n', obj.status_chain(ft_status))
