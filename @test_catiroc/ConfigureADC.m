function ConfigureADC(obj, address, contents)
% CONFIGUREADC - Updates configuration of ADC through SPI

%   CONFIGUREADC -
%   sends
%

if obj.GetIsOpen

    %% Send parameters to ADC
    obj.SendCommand(uint8([8, 0, address, contents, 255]));

    %% Read parameters from ADC
    obj.SendCommand(uint8([8, 1, address, 255]));

    %% Check if config is available in return
    AmountOfDataAvailable = obj.GetStatusUSB;
    if AmountOfDataAvailable ~= 1
        error('CONFIGUREADC: No data available in return')
    end

    %% Read it back
    [AmountBytesRead, BytesRead] = obj.ReadUSB;

    %% Check data returned
    if AmountBytesRead ~= 1
        error('CONFIGUREAAD: No configuration returned')
    end

    %% Check params sent is equal to params read
    if ~ isequal(BytesRead, contents)
        fprintf('\n\tCONFIGUREADC: parameter returned not equal to sent !!\n')
    else
        fprintf('\n\tConfiguration ADC OK.\n')
    end

else

    error('CONFIGUREADC: Device not open.')

end
