function [ElRead, DataRead] = ReadUSB(obj)
% READUSB - Reads data currently present in usb.

%   READUSB - Returns the amount of bytes and the actual bytes existing in bus
%
%   [ELREAD, DATAREAD] = READUSB() returns usb bus contents
%
%   ELREAD   - Amount of data read
%   DATAREAD - Array of bytes read

    if obj.GetIsOpen

        AmountInRxQueue = obj.GetStatusUSB;

        if AmountInRxQueue > 0

            if obj.GlobalParameters.verbose
                fprintf('\n\tBytes currently in Rx Queue is %i\n', AmountInRxQueue)
            end

            if AmountInRxQueue == 1
                InitData      = uint8(0);
                lpBufferPtr   = libpointer('voidPtr', InitData);
            else
                InitData      = uint8([5; 250*zeros(AmountInRxQueue-2,1); 255]);
                lpBufferPtr   = libpointer('voidPtr', InitData);
            end

            el_read_ptr = libpointer('ulongPtr',uint32(0));

            [status, ~, ElRead] = calllib('libftdi', 'ReadUSB', obj.DevNumber, ...
                                          lpBufferPtr, length(lpBufferPtr.Value), el_read_ptr);

            DataRead = get(lpBufferPtr, 'Value')';

            if status, fprintf('\n\tError: %s\n', obj.status_chain(status)), end

            if obj.GlobalParameters.verbose
                fprintf('\n\tBytes read %i\n', ElRead)
            end

        else

            fprintf('\n\tNo data available to read.\n')

        end

    else

        error('READUSB: Device not open.')

    end
