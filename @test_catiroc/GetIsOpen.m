function IsOpen = GetIsOpen(obj)
% GETISOPEN - Check if current peripheral is initialized

[ft_status, IsOpen] = calllib('libftdi', 'GetIsOpen', ...
                              obj.DevNumber, ...
                              libpointer('uint32Ptr', 0));

if obj.GlobalParameters.verbose
    fprintf('\n\tGetting isopen attribute ... %s\n', obj.status_chain(ft_status))
end
