classdef test_catiroc

% This class represents a ...

    properties %(SetAccess = private, GetAccess = private)

        DevNumber                     % device number as enumerated by usb bus : 0,1,...
        SerialNumber
        Description
        daq_mode                      % ciemat, daq, etc.
        configuration                 % struct equivalent to configuration file
        configuration_array           % decinal array equivalent to configuration file
        configuration_array_reference % decimal array with default config params
        pointer_type                  % for compatibility
        nb_asics                      % number of asics on board
        GlobalParameters              % global parameters

    end

    methods (Static)

        function SetFigure()
        % SETFIGURE - dsfkljl
        %
            legend('0','0','1','1','2','2','3','3','4','4','5','5','6','6','7','7','8','8',...
                   '9','9','10','10','11','11','12','12','13','13', ...
                   '14','14','15','15','Location', ...
                   'eastoutside')

        end

        function SetColor(Handle)
        % SETCOLOR - fdslkj
        %

            set(Handle(1), 'Color', 'b', 'LineStyle', '-')
            set(Handle(2), 'Color', 'b', 'LineStyle', '--')

            set(Handle(3), 'Color', 'r', 'LineStyle', '-')
            set(Handle(4), 'Color', 'r', 'LineStyle', '--')

            set(Handle(5), 'Color', 'g', 'LineStyle', '-')
            set(Handle(6), 'Color', 'g', 'LineStyle', '--')

            set(Handle(7), 'Color', 'c', 'LineStyle', '-')
            set(Handle(8), 'Color', 'c', 'LineStyle', '--')

            set(Handle(9), 'Color', 'm', 'LineStyle', '-')
            set(Handle(10), 'Color', 'm', 'LineStyle', '--')

            set(Handle(11), 'Color', 'y', 'LineStyle', '-')
            set(Handle(12), 'Color', 'y', 'LineStyle', '--')

            set(Handle(13), 'Color', 'k', 'LineStyle', '-')
            set(Handle(14), 'Color', 'k', 'LineStyle', '--')

            set(Handle(15), 'Color', [0.9290 0.6940 0.1250], 'LineStyle', '-')
            set(Handle(16), 'Color', [0.9290 0.6940 0.1250], 'LineStyle', '--')

            set(Handle(17), 'Color', 'b', 'LineStyle', '-')
            set(Handle(18), 'Color', 'b', 'LineStyle', '--')

            set(Handle(19), 'Color', 'r', 'LineStyle', '-')
            set(Handle(20), 'Color', 'r', 'LineStyle', '--')

            set(Handle(21), 'Color', 'g', 'LineStyle', '-')
            set(Handle(22), 'Color', 'g', 'LineStyle', '--')

            set(Handle(23), 'Color', 'c', 'LineStyle', '-')
            set(Handle(24), 'Color', 'c', 'LineStyle', '--')

            set(Handle(25), 'Color', 'm', 'LineStyle', '-')
            set(Handle(26), 'Color', 'm', 'LineStyle', '--')

            set(Handle(27), 'Color', 'y', 'LineStyle', '-')
            set(Handle(28), 'Color', 'y', 'LineStyle', '--')

            set(Handle(29), 'Color', [0.9290 0.6940 0.1250], 'LineStyle', '-')
            set(Handle(30), 'Color', [0.9290 0.6940 0.1250], 'LineStyle', '--')

            set(Handle(31), 'Color', [0.4940 0.1840 0.5560], 'LineStyle', '-')
            set(Handle(32), 'Color', [0.4940 0.1840 0.5560], 'LineStyle', '--')
        end
    end

    methods (Access = private)

% IsOpen = GetIsOpen(obj)
% PurgeUSB(obj)
        ConfigureADC(obj, address, contents)
% obj = UpdateConfig(obj, varargin)
        obj = RegisterToConfigArray_en_pp(obj, register, LowestBit)
        obj = RegisterToConfigArray_value(obj, register, LowestBit, Bits, Loop, revert)
        obj = RegisterToConfigArray_value_ext(obj, register, LowestBit, Bits, Loop, revert)

    end

    methods

        %% class contructor
        function obj = test_catiroc(DevNumber)
        % get_catiroc_parameters
            obj.DevNumber = DevNumber;
            obj.daq_mode  = 'catiroc';
            %% Must remain compatible under
            if strcmp('glnxa64',computer('arch'))
                obj.pointer_type = 'uint64Ptr';
            elseif strcmp('win64',computer('arch'))
                obj.pointer_type = 'uint32Ptr';
            else
                error('\n\tSYSTEM_TEST: Unknown system arch.\n')
            end

            %% nb of asics
            configtmp = GetGlobalParameters;
            obj.nb_asics = configtmp.nb_asics;

            %% initialize arrays
            for i=1:obj.nb_asics
                tmp =  struct('verbose', 0);
            end
            obj.configuration =  tmp;
        % command [CC, params, FF]
            obj.configuration_array = [204, zeros(1, 66*obj.nb_asics), 255];
            obj.configuration_array_reference = ...
                [ 204, repmat([215 15 180 68 2 0 144 0 0 0 0 0 4 132 85 45 145 10 21 193 48 76 19 4 193 48 76 19 4 193 48 76 19 4 193 48 76 19 4 0 0, ...
                               zeros(1,25)], 1, obj.nb_asics), 255];

            %% Global parameters
            obj.GlobalParameters = GetGlobalParameters;

            obj = obj.Initialize;
        end

        %% class destructor
        function delete(obj)

        %% Close USB
            if GetIsOpen(obj)

                ft_status = calllib('libftdi', 'FinalizeUSB', obj.DevNumber);

                if ft_status == 0
                    fprintf('\n\tDeleted catiroc object, dev. %i, SN %s, desc. %s\n',...
                            obj.DevNumber, obj.SerialNumber, obj.Description)
                else
                    fprintf('\n\tError deleting catiroc object, dev. %i, SN %s, desc. %s, status %s\n',...
                            obj.DevNumber, obj.SerialNumber, obj.Description, obj.status_chain(ft_status))
                end

            else

                fprintf('\n\tCatiroc object, dev. %i, SN %s, desc. %s not open.\n',...
                        obj.DevNumber, obj.SerialNumber, obj.Description)

            end

        end

    end

end % classdef
