function obj = GetADCData(obj, chgrp1, chgrp2, chgrp3, chgrp4, handles)
% GETADCDATA - Updates configuration of ADC through SPI

%   GETADCDATA -
%   sends
%

    AmountInRxQueue = handles.GlobalParameters.PacketSize;
    AmountOfEmptyDataPacketsReceived = 0;

    if obj.GetIsOpen

        obj = obj.ConfigureASIC(handles, {-1}, {0});

        %% Init Histograms

        %% Init Figures and plots

        nb_channels = 0;
        if chgrp1, nb_channels = nb_channels + 1; end
        if chgrp2, nb_channels = nb_channels + 1; end
        if chgrp3, nb_channels = nb_channels + 1; end
        if chgrp4, nb_channels = nb_channels + 1; end

        OscilloFigure = figure('Visible', 'off');
        %% OscilloFigureAxis = gca;
        set(gca, 'YLim',[-8192 8191])
        PlotOscilloFigure = plot(zeros(128,nb_channels,'int16'));
        title('Oscilloscope'), xlabel('Sample'), ylabel('ADC Channel')
        zoom on, grid minor
        if nb_channels == 1, legend('grp 1'), end
        if nb_channels == 2, legend('grp 1','grp 2'), end
        if nb_channels == 3, legend('grp 1','grp 2','grp 3'), end
        if nb_channels == 4, legend('grp 1','grp 2','grp 3','grp 4'), end

        %% Init buffers to allocate memory
        lpBufferPtr_big = libpointer('voidPtr', zeros(AmountInRxQueue, 1, 'uint8'));
        el_read_ptr     = libpointer('ulongPtr',uint32(0));

        %% ADC: Soft Reset
        obj.ConfigureADC(8, 3)

        %% ADC: Release Reset
        obj.ConfigureADC(8, 0)

%     %% ADC: Clear
%     obj.ConfigureADC(0, 36)

        %% Send command "07" to request data
        Command = uint8([7, uint8([ chgrp4*2^4+ chgrp3, chgrp2*2^4+ chgrp1]), 255]);
        obj.SendCommand(Command);

%     %% How much data to read ?
%     AmountInRxQueue = obj.GetStatusUSB;
%     if obj.configuration.verbose, fprintf('\n\tBytes in Rx Queue %i\n', AmountInRxQueue), end


        while get(gcbo,'Value')

            %% Read Data. Time out is 3 s.
            [status, ~, el_read] = calllib('libftdi', 'ReadUSB', ...
                                           obj.DevNumber, ...
                                           lpBufferPtr_big, ...
                                           AmountInRxQueue, ...
                                           el_read_ptr);

            %% ReadUSB returns ~= 0 if el_read ~= AmountInRxQueue
            if AmountOfEmptyDataPacketsReceived == 3 && status==50  % No data at all: stop
                fprintf('\n\tError: No data !!. \n\nStopped.\n'),
                return
            elseif status==50  % No data at all
                fprintf('\n\tError: No data !!\n'),
                AmountOfEmptyDataPacketsReceived = AmountOfEmptyDataPacketsReceived + 1;
                drawnow
                continue
            elseif status ~= 0 % low data
                fprintf('\n\tWarning: %s, bytes read is %i; ignoring data\n', obj.status_chain(status), el_read),
                drawnow
                continue
            end
            AmountOfEmptyDataPacketsReceived = 0;

            data = uint16(reshape(lpBufferPtr_big.Value(1:2*128*nb_channels),2,[]))';
            data = uint16(data(:,1)*2^8 + data(:,2));

            aa = double(data);
            aa(aa>8191) = aa(aa>8191) - 16384;
            aa = reshape(aa, [], nb_channels);
            for i = 1:nb_channels
                set(PlotOscilloFigure(i), 'YData', aa(:,i))
            end

% if get(handles.auto_scale_tag, 'Value')
%     set(OscilloFigureAxis, 'YLimMode','manual')
%     %set(OscilloFigureAxis, 'YLim',[-8192 8191])
% else
%     set(OscilloFigureAxis, 'YLimMode','auto')
% end

            set(OscilloFigure, 'Visible', 'on')
            drawnow

        end


% TODO:
%    - trames divisibles par X/(128*4*2),
%    - paquets plus petits, à fin de fluidifier l'affichage (X=65536, 16384, etc.)

    else

        error('GETADCDATA: Device not open.')

    end