function [SerialNumber, Description] = GetSerialDesc(obj)

    SerialNumberPtr = libpointer('int8Ptr',zeros(1,16,'int8'));
    DescriptionPtr  = libpointer('int8Ptr',zeros(1,64,'int8'));

    calllib('libftdi', 'GetDescriptors', SerialNumberPtr, DescriptionPtr);

    SerialNumber = char(uint8(get(SerialNumberPtr, 'Value')));
    Description  = char(uint8(get(DescriptionPtr, 'Value')));