function handles = system_initialize(handles)

    try
        handles.digi = system_test;
    catch
        handles.digi = -1;
    end