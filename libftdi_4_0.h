typedef unsigned long * LPDWORD;
typedef unsigned long DWORD;
typedef unsigned long ULONG;

// Init
ULONG InitializeUSB(int CardNb);
ULONG ConfigureUSB(int CardNb);
ULONG ConfigureBoard(int CardNb);

ULONG ResetPLL(int CardNb);
ULONG ResetDDRClock(int CardNb);
ULONG ResetMIG_IP(int CardNb);

// Get Infos
ULONG GetTriggerType(int CardNb, DWORD *myTriggerType);
ULONG GetDigitalThreshold(int CardNb, DWORD *myDigitalThreshold);
ULONG GetIsOpen(int CardNb, DWORD *myIsOpen);
ULONG GetControlRegister(int CardNb, char ControlRegisterOut[1]);
ULONG GetEscale(int CardNb, DWORD *myEscale);
ULONG GetFrequency(int CardNb, unsigned short int *myFrequency);
ULONG GetADCOffset(int CardNb, DWORD *myADCOffset);
ULONG GetVGA(int CardNb, int VGA_nb, DWORD *myVGA);
//
ULONG UpdateDACOffset(int CardNb, unsigned short int dac_command, unsigned short int dac_address, unsigned short int dac_value);
ULONG GetDACOffset_tr_ext_plus(int CardNb, unsigned long int *myDACOffset_tr_ext_plus);
ULONG GetDACOffset_tr_ext_minus(int CardNb, unsigned long int *myDACOffset_tr_ext_minus);
ULONG GetDACOffset_test_plus(int CardNb, unsigned long int *myDACOffset_test_plus);
ULONG GetDACOffset_test_minus(int CardNb, unsigned long int *myDACOffset_test_minus);
ULONG GetDACOffset_offset_plus(int CardNb, unsigned long int *myDACOffset_offset_plus);
ULONG GetDACOffset_offset_minus(int CardNb, unsigned long int *myDACOffset_offset_minus);
ULONG GetDACOffset_tr_plus(int CardNb, unsigned long int *myDACOffset_tr_plus);
ULONG GetDACOffset_tr_minus(int CardNb, unsigned long int *myDACOffset_tr_minus);
/* ULONG GetDACOffset_ref(int CardNb, unsigned long int *myDACOffset_ref); */
/* ULONG GetDACOffset_signal(int CardNb, unsigned long int *myDACOffset_signal); */
/* ULONG GetDACOffset_tr_int(int CardNb, unsigned long int *myDACOffset_tr_int); */
/* ULONG GetDACOffset_tr_ext(int CardNb, unsigned long int *myDACOffset_tr_ext); */
/* ULONG GetDACOffset_test(int CardNb, unsigned long int *myDACOffset_test); */
ULONG GetRxTxStatusUSB(int CardNb, LPDWORD myAmountInRxQueue, LPDWORD myAmountInTxQueue, LPDWORD myEventStatus);
ULONG GetNumberDevs(LPDWORD myNumberDevs);
ULONG GetDescriptors(char SerialNumberOut[16], char DescriptionOut[64]);
// Set Infos
ULONG SetTriggerType(int CardNb, DWORD myTriggerType);
ULONG SetDigitalThreshold(int CardNb, DWORD myDigitalThreshold);
// freq=500/Master, freq=250/Slave
ULONG SetFrequency(int CardNb, unsigned short int myFrequency);
ULONG SetFrequencyParams(int CardNb, const char MyParams[16]);
// 250, 500, 1000, 2000, 5000, 10000 (test signal)
ULONG SetRampMode(int CardNb);
ULONG SetUSBTestMode(int CardNb);
ULONG SetOscillogramMode(int CardNb, unsigned short int Length);
ULONG SetEscale(int CardNb, DWORD myEscale);
//
ULONG SetDACOffset_tr_ext_plus(int CardNb, unsigned long int myDACOffset_tr_ext_plus);
ULONG SetDACOffset_tr_ext_minus(int CardNb, unsigned long int myDACOffset_tr_ext_minus);
ULONG SetDACOffset_test_plus(int CardNb, unsigned long int myDACOffset_test_plus);
ULONG SetDACOffset_test_minus(int CardNb, unsigned long int myDACOffset_test_minus);
ULONG SetDACOffset_offset_plus(int CardNb, unsigned long int myDACOffset_offset_plus);
ULONG SetDACOffset_offset_minus(int CardNb, unsigned long int myDACOffset_offset_minus);
ULONG SetDACOffset_tr_plus(int CardNb, unsigned long int myDACOffset_tr_plus);
ULONG SetDACOffset_tr_minus(int CardNb, unsigned long int myDACOffset_tr_minus);
ULONG SetADCOffset(int CardNb, DWORD myADCOffset);
ULONG SetVGA(int CardNb, int VGA_nb, DWORD myVGA);
/* ULONG SetDACOffset_ref(int CardNb, unsigned long int myDACOffset_ref); */
/* ULONG SetDACOffset_signal(int CardNb, unsigned long int myDACOffset_signal); */
/* ULONG SetDACOffset_tr_int(int CardNb, unsigned long int myDACOffset_tr_int); */
/* ULONG SetDACOffset_tr_ext(int CardNb, unsigned long int myDACOffset_tr_ext); */
/* ULONG SetDACOffset_test(int CardNb, unsigned long int myDACOffset_test); */
ULONG ReadUSB(int CardNb, void * Buffer, DWORD BytesToRead, LPDWORD BytesRead);
// End
ULONG FinalizeUSB(int CardNb);
ULONG PurgeRxTxUSB(int CardNb);
ULONG WriteUSB(int CardNb, void * Buffer, DWORD BytesToWrite, LPDWORD BytesWritten);
