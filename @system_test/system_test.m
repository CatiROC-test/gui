classdef system_test
% This class represents a system of test cards, regardless of the ASIC under test

    properties %(SetAccess = private, GetAccess = private)

        NumberOfCards % Number of cards in the system
        CardDevNumber % array of device numbers as enumerated by corresponding bus : 0,1,...
        Cards         % handle to the cards

    end

    methods

        %% * class contructor
        function obj = system_test()

        %% Load library
            if ~libisloaded('libftdi')

                fprintf('\n\tloading library please wait ... ')

                if strcmp('win64',computer('arch'))

                    if isdeployed
                        %% when in stand alone mode
                        [notfound, warnings] = ...
                            loadlibrary('libftdi_x86_4_0', ...
                                        @libftdi_x86_4_0_prototype, ...
                                        'alias','libftdi');
                    else
                        [notfound, warnings] = ...
                            loadlibrary('libftdi_x86_4_0','libftdi_4_0.h', ...
                                        'alias','libftdi', ...
                                        'mfilename', ...
                                        'libftdi_x86_4_0_prototype');
                    end

                elseif strcmp('glnxa64',computer('arch'))

                    if isdeployed
                        %% when in stand alone mode
                        [notfound, warnings] = ...
                            loadlibrary('libftdi_linux',@libftdi_linux_prototype, ...
                                        'alias','libftdi');
                    else
                        [notfound, warnings] = ...
                            loadlibrary('libftdi_linux','libftdi_4_0.h', ...
                                        'alias','libftdi', ...
                                        'mfilename', ...
                                        'libftdi_linux_prototype');
                    end

                else

                    error('\n\tSYSTEM_TEST: Unknown system arch.\n')

                end

                fprintf('\t ... Done.\n')

            else

                fprintf('\n\tLibrary already loaded.\n')

            end

            %% Get the number of cards present
            numDevsPtr  = libpointer('ulongPtr',uint32(0));
            [ft_status, ~] = calllib('libftdi', 'GetNumberDevs', numDevsPtr);
            if ft_status ~= 0
                error('\n\tSYSTEM_TEST: error %i numerating devices.\n', ft_status)
            end
            obj.NumberOfCards = get(numDevsPtr, 'Value');
            fprintf('\n\tInit USB ... detecting system, %i devices detected: \n', obj.NumberOfCards)

            %% Initialize cards
            for i = 1:obj.NumberOfCards
                fprintf('\n\t####################\n')
                fprintf('\t  Card Dev. Nb. %i\n', i-1)
                fprintf('\t####################\n')
                obj.CardDevNumber(i) = i-1;
                tmp(i) = test_catiroc(i-1);
            end
            obj.Cards = tmp;

        end

        %% class destructor
        function delete(obj)

            fprintf('\n\tDeleting system catiroc ...\n')

            for i = 1:obj.NumberOfCards
                obj.Cards(i).delete;
            end

            if libisloaded('libftdi')
                unloadlibrary('libftdi')
                fprintf('\n\tLibrary unloaded.')
            end

            fprintf('\n\t                           ... Done.\n')

        end

    end % methods

end % classdef
